namespace es
	using im, chemistry, ses, soil, ecology;
	
process FloodRegulation
	"Flood regulation lowers flood hazards caused by heavy precipitation 
     events by reducing the runoff fraction. Vegetation, with the retaining effect of its 
     earth cover and roots, is the primary natural asset responsible for the benefit."
	is ses:RegulatingEcosystemBenefit im:TransitiveProcess 
	   with (im:Retained soil:Soil im:Mass caused by ecology:Vegetation);
	 
process LandslideRegulation
	"Landslide regulation lowers landslide hazards caused by heavy precipitation 
     events by reducing the runoff fraction. Vegetation, with the retaining effect of its 
     earth cover and roots, is the primary natural asset responsible for the benefit."
	is ses:RegulatingEcosystemBenefit im:TransitiveProcess 
	   with (im:Retained soil:Soil im:Mass caused by ecology:Vegetation);
	   
process ClimateRegulation
	"Climate regulation regulates processes related to atmospheric chemical composition, 
     the greenhouse effect, the ozone layer, precipitation, air quality, and moderation of 
     temperature and weather patterns, including cloud formation."
	is ses:RegulatingEcosystemBenefit im:TransitiveProcess 
	   with (chemistry:Organic chemistry:Carbon im:Mass caused by ecology:Vegetation);	   
	   
configuration GreenInfrastructure
	"The system of landscape elements attributable to nature, seen as an object of 
     conservation and management for the provision of services to society."
	is ecology:Landscape;
	