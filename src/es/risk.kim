namespace es.risk
	"Semantic articulation of risks pertaining to environmental factors"
	using im, earth, demography, infrastructure, risk;

event ExtremeHeatEvent 
	is im:PeriodExpiration with im:High earth:AtmosphericTemperature 
		within earth:Location;

quantity HeatVulnerabilityMetric
	describes risk:VulnerabilityMetric probability of im:Collapse of demography:SocialGroup
		caused by ExtremeHeatEvent
    increases with im:Height of infrastructure:Building;
	
quantity HeatExposureMetric
	describes risk:ExposureMetric probability of im:Collapse of demography:SocialGroup 
		caused by ExtremeHeatEvent
    increases with count of demography:HumanIndividual;
	
quality HeatRiskMetric
	is risk:RiskMetric probability of im:Collapse of demography:SocialGroup
		caused by ExtremeHeatEvent;

quality HeatHazardMetric
	is risk:HazardMetric earth:AtmosphericTemperature during ExtremeHeatEvent;

configuration HeatRisk is risk:Risk
	emerges from ExtremeHeatEvent
	implies HeatVulnerabilityMetric for risk:VulnerabilityMetric,
			HeatExposureMetric for risk:ExposureMetric,
			HeatHazardMetric for risk:HazardMetric,
			HeatRiskMetric for risk:RiskMetric;
      
	